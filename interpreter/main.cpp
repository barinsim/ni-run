#include <iostream>
#include "AST.h"
#include "ASTEvaluator.h"

int main() {
    ASTBuilder builder;
    auto ast = builder.fromJsonStream(std::cin);
    ASTEvaluator eval;
    ast->accept(eval);
}
