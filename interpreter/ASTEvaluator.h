#ifndef INTERPRETER_ASTEVALUATOR_H
#define INTERPRETER_ASTEVALUATOR_H

#include"ASTVisitor.h"

#include<variant>
#include<list>
#include<unordered_map>

#include"Value.h"


struct Environment {
    std::shared_ptr<Environment> parent;
    std::unordered_map<std::string, ValueRef> locals;
    std::unordered_map<std::string, Function*> functions;
};


class ASTEvaluator : public ASTVisitor
{
public:
    void visit(Print&) override;
    void visit(Integer&) override;
    void visit(Boolean&) override;
    void visit(Null&) override;
    void visit(Variable&) override;
    void visit(Top&) override;
    void visit(AccessVariable&) override;
    void visit(AssignVariable&) override;
    void visit(Function&) override;
    void visit(CallFunction&) override;
    void visit(Block&) override;
    void visit(Loop&) override;
    void visit(Conditional&) override;
    void visit(Object&) override;
    void visit(Array&) override;
    void visit(AssignField&) override;
    void visit(AssignArray&) override;
    void visit(AccessField&) override;
    void visit(AccessArray&) override;
    void visit(CallMethod&) override;

public:
    ValueRef eval(AST*);

private:
    void setReturnVal(const ValueRef& retv) { returnValue = retv; }
    ValueRef returnVal() const { return returnValue; }
    std::shared_ptr<Environment> enterNewContext();
    void leaveContext(std::shared_ptr<Environment> newContext);
    void enterNewEnv();
    void leaveEnv();
    bool shouldContinue(AST* cond);

    ValueRef findMethodReceiver(ValueRef val, const std::string& identifier);
    void handleObjectMethodCall(ValueRef receiver, const std::vector<ValueRef>& args, const std::string& identifier);
    void handleBoolMethodCall(ValueRef receiver, const std::vector<ValueRef>& args, const std::string& identifier);
    void handleIntMethodCall(ValueRef receiver, const std::vector<ValueRef>& args, const std::string& identifier);
    void handleNullMethodCall(ValueRef receiver, const std::vector<ValueRef>& args, const std::string& identifier);

private:
    ValueRef returnValue;
    std::shared_ptr<Environment> env;
    std::shared_ptr<Environment> globalEnv;
};

#endif //INTERPRETER_ASTEVALUATOR_H
