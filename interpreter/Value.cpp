#include "Value.h"


std::string ValueStringifier::operator()(const ObjectValue& obj) {
    std::string str = "object";
    str += "(";
    str += std::string("..=") + obj.parent.toString();
    for (auto& p : obj.variables) {
        str += ", ";
        str += p.first + "=" + p.second.toString();
    }
    str += ")";
    return str;
}

std::string ValueStringifier::operator()(const IntegerValue &integer) {
    return std::to_string(integer.val);
}

std::string ValueStringifier::operator()(const BooleanValue &boolean) {
    return boolean.val ? "true" : "false";
}

std::string ValueStringifier::operator()(const ArrayValue& arr) {
    std::string str;
    str += "[";
    bool first = true;
    for (auto& ref : arr.values) {
        if (!first) {
            str += ", ";
        }
        first = false;
        str += ref.toString();
    }
    str += "]";
    return str;
}

std::string ValueStringifier::operator()(const UnitValue &) {
    return "null";
}

std::string ValueRef::toString() const {
    ValueStringifier visitor;
    return std::visit(visitor, *ref);
}