#include "ASTEvaluator.h"

#include<optional>

#include "AST.h"
#include "Value.h"

void ASTEvaluator::visit(Print& print)
{
    auto isValid = [](const std::string& fmt) {
        if (fmt.empty()) { return true; }
        if (fmt.size() >= 2 && fmt.at(fmt.size() - 1) == '\\' && fmt.at(fmt.size() - 2) != '\\') {
            return false;
        }
        static constexpr std::array allowed = {'\\', 'n', '"', 'r', '~', 't'};
        std::optional<char> prev;
        for (char c : fmt) {
            auto pred = [c](char x){ return x == c; };
            if (prev == '\\' && std::none_of(allowed.begin(), allowed.end(), pred)) {
                return false;
            }
            prev = c;
        }
        return true;
    };

    if (!isValid(print.format)) {
        throw std::domain_error("Invalid format"); // TODO more specific
    }
    auto argsIt = print.args.begin();
    std::optional<char> prev;
    std::string str;
    for (char c : print.format) {
        if (prev != '\\' && c == '\\') {
            // pass
        }
        else if (prev == '\\') {
            switch (c) {
                case '~' : str += '~'; break;
                case 'n' : str += '\n'; break;
                case '"' : str += '"'; break;
                case 'r' : str += '\r'; break;
                case 't' : str += '\t'; break;
                case '\\' : str += '\\'; break;
                default : throw std::domain_error("weird");
            }
        }
        else if (c == '~') {
            str += eval(argsIt->get()).toString();
            ++argsIt;
        } else {
            str += c;
        }
        prev = c;
    }
    std::cout << str.c_str();
    std::cout.flush();
    setReturnVal(createValue<UnitValue>());
}

void ASTEvaluator::visit(Integer& integer) {
    setReturnVal(createValue<IntegerValue>(integer.val));
}

void ASTEvaluator::visit(Boolean& boolean) {
    setReturnVal(createValue<BooleanValue>(boolean.val));
}

void ASTEvaluator::visit(Null& null) {
    setReturnVal(createValue<UnitValue>());
}

void ASTEvaluator::visit(Variable& var) {
    ValueRef valRef = eval(var.value.get());
    if (env->locals.count(var.identifier)) {
        throw std::domain_error("variable redefinition");
    }
    env->locals.insert({var.identifier, valRef});
    setReturnVal(valRef);
}

void ASTEvaluator::visit(Top& top) {
    globalEnv = std::make_shared<Environment>();
    env = globalEnv;
    for (auto& stmt : top.stmts) {
        stmt->accept(*this);
    }
}

void ASTEvaluator::visit(AccessVariable& var) {
    Environment* e = env.get();
    while (e && !e->locals.count(var.identifier)) {
        e = e->parent.get();
    }
    if (!e) {
        throw std::domain_error("undefined variable");
    }
    setReturnVal(e->locals.at(var.identifier));
}

void ASTEvaluator::visit(AssignVariable& var) {
    Environment* e = env.get();
    while (e && !e->locals.count(var.identifier)) {
        e = e->parent.get();
    }
    if (!e) {
        throw std::domain_error("undefined variable");
    }
    ValueRef newValRef = eval(var.value.get());
    e->locals.at(var.identifier) = newValRef;
    setReturnVal(newValRef);
}

void ASTEvaluator::visit(Function& function) {
    auto it = globalEnv->functions.find(function.identifier);
    if (it != globalEnv->functions.end()) {
        throw std::domain_error("function redefinition");
    }
    globalEnv->functions.insert({function.identifier, &function});
    setReturnVal(createValue<UnitValue>());
}

void ASTEvaluator::visit(CallFunction& function) {
    auto it = globalEnv->functions.find(function.identifier);
    if (it == globalEnv->functions.end()) {
        throw std::domain_error("calling undefined function");
    }
    std::vector<ValueRef> evaluatedArgs;
    for (auto& arg : function.args) {
        evaluatedArgs.push_back(eval(arg.get()));
    }
    auto oldContext = enterNewContext();
    Function* ast = it->second;
    auto argsIt = evaluatedArgs.begin();
    for (auto& param : ast->parameters) {
        env->locals.insert({param, *argsIt});
        ++argsIt;
    }
    ValueRef retVal = eval(ast->body.get());
    leaveContext(oldContext);
    setReturnVal(retVal);
}

void ASTEvaluator::visit(Block& block) {
    setReturnVal(createValue<UnitValue>());
    enterNewEnv();
    for (auto& expr : block.exprs) {
        eval(expr.get());
    }
    leaveEnv();
}

void ASTEvaluator::visit(Loop& loop) {
    while (shouldContinue(loop.condition.get())) {
        eval(loop.body.get());
    }
    setReturnVal(createValue<UnitValue>());
}

void ASTEvaluator::visit(Conditional& conditional) {
    if (shouldContinue(conditional.condition.get())) {
        eval(conditional.tBody.get());
    } else {
        eval(conditional.fBody.get());
    }
}

ValueRef ASTEvaluator::eval(AST* node) {
    node->accept(*this);
    return returnVal();
}

std::shared_ptr<Environment> ASTEvaluator::enterNewContext() {
    auto newEnv = std::make_shared<Environment>();
    newEnv->parent = globalEnv;
    std::shared_ptr<Environment> oldEnv = env;
    env = newEnv;
    return oldEnv;
}

void ASTEvaluator::leaveContext(std::shared_ptr<Environment> newContext) {
    env = newContext;
}

void ASTEvaluator::enterNewEnv() {
    auto newEnv = std::make_shared<Environment>();
    newEnv->parent = env;
    env = newEnv;
}

void ASTEvaluator::leaveEnv() {
    env = env->parent;
}

bool ASTEvaluator::shouldContinue(AST* cond) {
    ValueRef condVal = eval(cond);
    if (auto val = condVal.to<BooleanValue>()) {
        return val->val;
    }
    if (auto val = condVal.to<UnitValue>()) {
        return false;
    }
    return true;
};

void ASTEvaluator::visit(Object& obj) {
    std::unordered_map<std::string, Function*> methods;
    std::unordered_map<std::string, ValueRef> variables;
    for (auto& decl : obj.decls) {
        if (auto var = decl->to<Variable>()) {
            if (variables.count(var->identifier)) {
                throw std::domain_error("field redefinition");
            }
            variables.insert({var->identifier, eval(var->value.get())});
        }
        else if (auto func = decl->to<Function>()) {
            if (methods.count(func->identifier)) {
                throw std::domain_error("method redefinition");
            }
            methods.insert({func->identifier, func});
        }
        else {
            throw std::domain_error("members can only be variables or methods");
        }
    }
    ValueRef parent = eval(obj.parent.get());
    setReturnVal(createValue<ObjectValue>(methods, variables, parent));
}

void ASTEvaluator::visit(Array& arr) {
    ValueRef sizeRef = eval(arr.size.get());
    auto sizeVal = sizeRef.to<IntegerValue>();
    if (!sizeVal || sizeVal->val <= 0) {
        throw std::domain_error("invalid array size");
    }
    int size = sizeVal->val;
    std::vector<ValueRef> values;
    while (size--) {
        enterNewEnv();
        values.push_back(eval(arr.value.get()));
        leaveEnv();
    }
    setReturnVal(createValue<ArrayValue>(values));
}

void ASTEvaluator::visit(AssignField& field) {
    auto objRef = eval(field.object.get());
    auto obj = objRef.to<ObjectValue>();
    if (!obj) {
        throw std::domain_error("trying to assign member of a non-object value");
    }
    auto it = obj->variables.find(field.identifier);
    if (it == obj->variables.end()) {
        throw std::domain_error("trying to assign unknown field");
    }
    ValueRef val = eval(field.value.get());
    it->second = val;
    setReturnVal(val);
}

void ASTEvaluator::visit(AssignArray& arr) {
   auto arrayRef = eval(arr.array.get());
   auto array = arrayRef.to<ArrayValue>();
   if (!array) {
       throw std::domain_error("array assignment into non-array value");
   }
   auto indexRef = eval(arr.index.get());
   auto index = indexRef.to<IntegerValue>();
   if (!index || index->val < 0 || index->val >= (int)array->values.size()) {
       throw std::domain_error("index array must be non-negative integer value within bounds");
   }
   ValueRef val = eval(arr.value.get());
   array->values.at(index->val) = val;
   setReturnVal(val);
}

void ASTEvaluator::visit(AccessField& field) {
    auto objRef = eval(field.object.get());
    auto obj = objRef.to<ObjectValue>();
    if (!obj) {
        throw std::domain_error("trying to access member of a non-object value");
    }
    auto it = obj->variables.find(field.identifier);
    if (it == obj->variables.end()) {
        throw std::domain_error("trying to access unknown field");
    }
    setReturnVal(it->second);
}

void ASTEvaluator::visit(AccessArray& arr) {
    auto arrayRef = eval(arr.array.get());
    auto array = arrayRef.to<ArrayValue>();
    if (!array) {
        throw std::domain_error("array access into a non-array value");
    }
    auto indexRef = eval(arr.index.get());
    auto index = indexRef.to<IntegerValue>();
    if (!index || index->val < 0 || index->val >= array->values.size()) {
        throw std::domain_error("index array must be a non-negative integer value within bounds");
    }
    setReturnVal(array->values.at(index->val));
}

void ASTEvaluator::visit(CallMethod& method) {
    ValueRef obj = eval(method.object.get());
    std::vector<ValueRef> args;
    for (auto& argAst : method.args) {
        args.push_back(eval(argAst.get()));
    }
    auto receiver = findMethodReceiver(obj, method.identifier);
    if (receiver.to<ObjectValue>()) {
        handleObjectMethodCall(receiver, args, method.identifier);
    }
    else if (receiver.to<IntegerValue>()) {
        handleIntMethodCall(receiver, args, method.identifier);
    }
    else if (receiver.to<BooleanValue>()) {
        handleBoolMethodCall(receiver, args, method.identifier);
    }
    else if (receiver.to<UnitValue>()) {
        handleNullMethodCall(receiver, args, method.identifier);
    }
    else if (receiver.to<ArrayValue>()) {
        throw std::domain_error("cannot call method with an array as a receiver");
    }
    else {
        throw std::domain_error("weird method dispatch");
    }
}

ValueRef ASTEvaluator::findMethodReceiver(ValueRef val, const std::string &identifier) {
    ValueRef receiver = val;
    auto* tmp = receiver.to<ObjectValue>();
    while (tmp && !tmp->methods.count(identifier)) {
        receiver = tmp->parent;
        tmp = tmp->parent.to<ObjectValue>();
    }
    return receiver;
}

void ASTEvaluator::handleObjectMethodCall(ValueRef receiver,
                                          const std::vector<ValueRef> &args,
                                          const std::string &identifier) {
    auto obj = receiver.to<ObjectValue>();
    Function* methodAst = obj->methods.at(identifier);
    auto oldContext = enterNewContext();
    for (int i = 0; i < methodAst->parameters.size(); ++i) {
        env->locals.insert({methodAst->parameters.at(i), args.at(i)});
    }
    env->locals.insert({"this", receiver});
    eval(methodAst->body.get());
    leaveContext(oldContext);
}

void ASTEvaluator::handleBoolMethodCall(ValueRef receiver, const std::vector<ValueRef> &args,
                                        const std::string &identifier) {
    static const std::array<std::string, 4> allowed = {"==", "!=", "&", "|"};
    auto cmp = [&](auto& op) { return op == identifier; };
    if (args.size() != 1 || std::none_of(allowed.begin(), allowed.end(), cmp)) {
        throw std::domain_error("unsupported bool operation");
    }
    ValueRef arg = args.at(0);
    bool lhs = receiver.to<BooleanValue>()->val;
    bool rhs;
    if (auto b = arg.to<BooleanValue>()) {
        rhs = b->val;
    } else if (identifier == "==" || identifier == "!=") {
        setReturnVal(createValue<BooleanValue>(identifier == "!="));
        return;
    } else {
        throw std::domain_error("unsupported bool operand");
    }
    bool res = false;
    if (identifier == "==") { res = lhs == rhs; }
    if (identifier == "!=") { res = lhs != rhs; }
    if (identifier == "&") { res = lhs && rhs; }
    if (identifier == "|") { res = lhs || rhs; }
    setReturnVal(createValue<BooleanValue>(res));
}

void ASTEvaluator::handleIntMethodCall(ValueRef receiver, const std::vector<ValueRef> &args,
                                  const std::string &identifier) {
    static const std::array<std::string, 11> allowed =
            {"==", "!=", "+", "-", "*", "/", "%", "<=", ">=", "<", ">"};
    auto cmp = [&](auto& op) { return op == identifier; };
    if (args.size() != 1 || std::none_of(allowed.begin(), allowed.end(), cmp)) {
        throw std::domain_error("unsupported integer operation");
    }
    ValueRef arg = args.at(0);
    int lhs = receiver.to<IntegerValue>()->val;
    int rhs;
    if (auto i = arg.to<IntegerValue>()) {
        rhs = i->val;
    } else if (identifier == "==" || identifier == "!=") {
        setReturnVal(createValue<IntegerValue>(identifier == "!="));
        return;
    } else {
        throw std::domain_error("unsupported integer operand");
    }
    std::variant<int, bool> res;
    if (identifier == "==") { res = lhs == rhs; }
    if (identifier == "!=") { res = lhs != rhs; }
    if (identifier == "<") { res = lhs < rhs; }
    if (identifier == ">") { res = lhs > rhs; }
    if (identifier == "<=") { res = lhs <= rhs; }
    if (identifier == ">=") { res = lhs >= rhs; }
    if (identifier == "+") { res = lhs + rhs; }
    if (identifier == "-") { res = lhs - rhs; }
    if (identifier == "*") { res = lhs * rhs; }
    if (identifier == "/") { res = lhs / rhs; }
    if (identifier == "%") { res = lhs % rhs; }
    if (std::holds_alternative<int>(res)) {
        setReturnVal(createValue<IntegerValue>(std::get<int>(res)));
    } else {
        setReturnVal(createValue<BooleanValue>(std::get<bool>(res)));
    }
}

void ASTEvaluator::handleNullMethodCall(ValueRef receiver, const std::vector<ValueRef> &args,
                                        const std::string &identifier) {
    static const std::array<std::string, 2> allowed = {"==", "!="};
    auto cmp = [&](auto& op) { return op == identifier; };
    if (args.size() != 1 || std::none_of(allowed.begin(), allowed.end(), cmp)) {
        throw std::domain_error("unsupported unit operation");
    }
    ValueRef arg = args.at(0);
    bool res = false;
    if (arg.to<UnitValue>()) {
        res = identifier == "==";
    } else {
        res = identifier == "!=";
    }
    setReturnVal(createValue<BooleanValue>(res));
}



