#ifndef INTERPRETER_VALUE_H
#define INTERPRETER_VALUE_H

#include <variant>
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>

class Function;
class ValueRef;
class ObjectValue;
class IntegerValue;
class BooleanValue;
class BooleanValue;
class ArrayValue;
class UnitValue;

class ValueStringifier {
public:
    std::string operator()(const ObjectValue&);
    std::string operator()(const IntegerValue& integer);
    std::string operator()(const BooleanValue& boolean);
    std::string operator()(const ArrayValue&);
    std::string operator()(const UnitValue&);
};

using Value = std::variant<ObjectValue, IntegerValue, BooleanValue, ArrayValue, UnitValue>;

class ValueRef
{
public:
    template <typename T>
    T* to() const&;
    template <typename T>
    T* to() && = delete;
    std::string toString() const;

private:
    std::shared_ptr<Value> ref;

    template <typename T, typename ...Args>
    friend ValueRef createValue(Args&& ...args);
};

template <typename T, typename ...Args>
ValueRef createValue(Args&& ...args) {
    ValueRef vr;
    vr.ref = std::make_shared<Value>(std::in_place_type<T>, std::forward<Args>(args)...);
    return vr;
}

class ObjectValue
{
public:
    ObjectValue(std::unordered_map<std::string, Function*> m,
                std::unordered_map<std::string, ValueRef> v,
                ValueRef p)
                : methods(std::move(m)), variables(std::move(v)), parent(std::move(p)) {}

    ValueRef parent;
    std::unordered_map<std::string, Function*> methods;
    std::unordered_map<std::string, ValueRef> variables;
};

class ArrayValue {
public:
    ArrayValue(std::vector<ValueRef> v) : values(std::move(v)) {}
public:
    std::vector<ValueRef> values;
};

class IntegerValue
{
public:
    IntegerValue(int v = 0) : val(v) {}
public:
    int val;
};

class BooleanValue {
public:
    BooleanValue(bool b = true) : val(b) {}
public:
    bool val;
};

class UnitValue {};

template <typename T>
T* ValueRef::to() const& {
    if (std::holds_alternative<T>(*ref)) {
        return &std::get<T>(*ref);
    }
    return nullptr;
}



#endif //INTERPRETER_VALUE_H
