#ifndef INTERPRETER_AST_H
#define INTERPRETER_AST_H

#include<vector>
#include<iostream>

#include "json.hpp"
#include "ASTVisitor.h"

using json = nlohmann::json;

class AST
{
public:
    virtual ~AST() = default;
    virtual void accept(ASTVisitor& visitor) { throw std::domain_error("not_implemented"); }

    template <typename T>
    T* to() {
        return dynamic_cast<T*>(this);
    }
};

using ASTPtr = std::unique_ptr<AST>;

class Top : public AST
{
public:
    std::vector<ASTPtr> stmts;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Integer : public AST
{
public:
    int val;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Boolean : public AST
{
public:
    bool val;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Null : public AST
{
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Variable : public AST
{
public:
    std::string identifier;
    ASTPtr value;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class AccessVariable : public AST
{
public:
    std::string identifier;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class AssignVariable : public AST
{
public:
    std::string identifier;
    ASTPtr value;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Function : public AST
{
public:
    std::string identifier;
    std::vector<std::string> parameters;
    ASTPtr body;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class CallFunction : public AST
{
public:
    std::string identifier;
    std::vector<ASTPtr> args;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Print : public AST
{
public:
    std::string format;
    std::vector<ASTPtr> args;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Block : public AST
{
public:
    std::vector<ASTPtr> exprs;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Loop : public AST
{
public:
    ASTPtr condition;
    ASTPtr body;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Conditional : public AST
{
public:
    ASTPtr condition;
    ASTPtr tBody;
    ASTPtr fBody;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class Object : public AST
{
public:
    ASTPtr parent;
    std::vector<ASTPtr> decls;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }

};

class Array : public AST
{
public:
    ASTPtr size;
    ASTPtr value;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class AccessArray : public AST
{
public:
    ASTPtr array;
    ASTPtr index;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class AssignArray : public AST
{
public:
    ASTPtr array;
    ASTPtr index;
    ASTPtr value;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class AssignField : public AST
{
public:
    ASTPtr object;
    std::string identifier;
    ASTPtr value;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class AccessField : public AST
{
public:
    ASTPtr object;
    std::string identifier;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class CallMethod : public AST
{
public:
    ASTPtr object;
    std::string identifier;
    std::vector<ASTPtr> args;
public:
    void accept(ASTVisitor& visitor) override { visitor.visit(*this); }
};

class ASTBuilder
{
public:
    ASTPtr fromJsonStream(std::istream& is) {
        json j;
        is >> j;
        return fromJson(j);
    }

    ASTPtr fromJson(const json& j)
    {
        if (j.is_object() && j.contains("Integer")) {
            return fromJson<Integer>(j);
        }
        else if (j.is_object() && j.contains("Boolean")) {
            return fromJson<Boolean>(j);
        }
        else if (j.is_string() && j.get<std::string>() == "Null") {
            return fromJson<Null>(j);
        }
        else if (j.is_object() && j.contains("Variable")) {
            return fromJson<Variable>(j);
        }
        else if (j.is_object() && j.contains("Top")) {
            return fromJson<Top>(j);
        }
        else if (j.is_object() && j.contains("AccessVariable")) {
            return fromJson<AccessVariable>(j);
        }
        else if (j.is_object() && j.contains("AssignVariable")) {
            return fromJson<AssignVariable>(j);
        }
        else if (j.is_object() && j.contains("Function")) {
            return fromJson<Function>(j);
        }
        else if (j.is_object() && j.contains("CallFunction")) {
            return fromJson<CallFunction>(j);
        }
        else if (j.is_object() && j.contains("Print")) {
            return fromJson<Print>(j);
        }
        else if (j.is_object() && j.contains("Block")) {
            return fromJson<Block>(j);
        }
        else if (j.is_object() && j.contains("Loop")) {
            return fromJson<Loop>(j);
        }
        else if (j.is_object() && j.contains("Conditional")) {
            return fromJson<Conditional>(j);
        }
        else if (j.is_object() && j.contains("Object")) {
            return fromJson<Object>(j);
        }
        else if (j.is_object() && j.contains("AssignField")) {
            return fromJson<AssignField>(j);
        }
        else if (j.is_object() && j.contains("AccessField")) {
            return fromJson<AccessField>(j);
        }
        else if (j.is_object() && j.contains("CallMethod")) {
            return fromJson<CallMethod>(j);
        }
        else if (j.is_object() && j.contains("Array")) {
            return fromJson<Array>(j);
        }
        else if (j.is_object() && j.contains("AccessArray")) {
            return fromJson<AccessArray>(j);
        }
        else if (j.is_object() && j.contains("AssignArray")) {
            return fromJson<AssignArray>(j);
        }
        throw std::exception(); // TODO something more concrete
    }

    template <typename T>
    ASTPtr fromJson(const json& j) {
        auto node = std::make_unique<T>();
        fromJson(j, *node.get());
        return node;
    }

private:
    void fromJson(const json& j, Top& top) {
        for (auto& [key, val] : j.at("Top").items()) {
            top.stmts.push_back(fromJson(val));
        }
    }

    void fromJson(const json& j, Integer& integer) {
        integer.val = j.at("Integer").get<int>();
    }

    void fromJson(const json& j, Boolean& boolean) {
        boolean.val = j.at("Boolean").get<bool>();
    }

    void fromJson(const json& j, Null& null) {
        // pass
    }

    void fromJson(const json& j, Variable& variable) {
        auto val = j.at("Variable");
        variable.identifier = val.at("name").get<std::string>();
        variable.value = fromJson(val.at("value"));
    }

    void fromJson(const json& j, AccessVariable& variable) {
        auto val = j.at("AccessVariable");
        variable.identifier = val.at("name").get<std::string>();
    }

    void fromJson(const json& j, AssignVariable& variable) {
        auto val = j.at("AssignVariable");
        variable.identifier = val.at("name").get<std::string>();
        variable.value = fromJson(val.at("value"));
    }

    void fromJson(const json& j, Function& function) {
        auto val = j.at("Function");
        function.identifier = val.at("name").get<std::string>();
        function.body = fromJson(val.at("body"));
        for (auto& elem : val.at("parameters")) {
            function.parameters.push_back(elem.get<std::string>());
        }
    }

    void fromJson(const json& j, CallFunction& function) {
        auto val = j.at("CallFunction");
        function.identifier = val.at("name").get<std::string>();
        for (auto& elem : val.at("arguments")) {
            function.args.push_back(fromJson(elem));
        }
    }

    void fromJson(const json& j, Print& print) {
        auto val = j.at("Print");
        print.format = val.at("format").get<std::string>();
        for (auto& elem : val.at("arguments")) {
            print.args.push_back(fromJson(elem));
        }
    }

    void fromJson(const json& j, Block& block) {
        for (auto& elem : j.at("Block")) {
            block.exprs.push_back(fromJson(elem));
        }
    }

    void fromJson(const json& j, Loop& loop) {
        auto val = j.at("Loop");
        loop.condition = fromJson(val.at("condition"));
        loop.body = fromJson(val.at("body"));
    }

    void fromJson(const json& j, Conditional& conditional) {
        auto val = j.at("Conditional");
        conditional.condition = fromJson(val.at("condition"));
        conditional.tBody = fromJson(val.at("consequent"));
        conditional.fBody = fromJson(val.at("alternative"));
    }

    void fromJson(const json& j, Object& obj) {
        auto val = j.at("Object");
        obj.parent = fromJson(val.at("extends"));
        for (auto& member : val.at("members")) {
            obj.decls.push_back(fromJson(member));
        }
    }

    void fromJson(const json& j, AssignField& field) {
        auto val = j.at("AssignField");
        field.object = fromJson(val.at("object"));
        field.identifier = val.at("field").get<std::string>();
        field.value = fromJson(val.at("value"));
    }

    void fromJson(const json& j, AccessField& field) {
        auto val = j.at("AccessField");
        field.object = fromJson(val.at("object"));
        field.identifier = val.at("field").get<std::string>();
    }

    void fromJson(const json& j, CallMethod& method) {
        auto val = j.at("CallMethod");
        method.object = fromJson(val.at("object"));
        method.identifier = val.at("name").get<std::string>();
        for (auto& arg : val.at("arguments")) {
            method.args.push_back(fromJson(arg));
        }
    }

    void fromJson(const json& j, Array& arr) {
        auto val = j.at("Array");
        arr.size = fromJson(val.at("size"));
        arr.value = fromJson(val.at("value"));
    }

    void fromJson(const json& j, AccessArray& arr) {
        auto val = j.at("AccessArray");
        arr.array = fromJson(val.at("array"));
        arr.index = fromJson(val.at("index"));
    }

    void fromJson(const json& j, AssignArray& arr) {
        auto val = j.at("AssignArray");
        arr.array = fromJson(val.at("array"));
        arr.index = fromJson(val.at("index"));
        arr.value = fromJson(val.at("value"));
    }
};


#endif //INTERPRETER_AST_H
