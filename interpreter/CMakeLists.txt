cmake_minimum_required(VERSION 3.0)
project(interpreter)

set(CMAKE_CXX_STANDARD 17)

add_executable(interpreter main.cpp AST.h ASTEvaluator.h ASTEvaluator.cpp ASTVisitor.h Value.h Value.cpp)

include_directories("deps")
