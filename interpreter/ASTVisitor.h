#ifndef INTERPRETER_ASTVISITOR_H
#define INTERPRETER_ASTVISITOR_H

#include<memory>

class AST;
using ASTPtr = std::unique_ptr<AST>;
class Print;
class Integer;
class Boolean;
class Null;
class Variable;
class Top;
class AccessVariable;
class AssignVariable;
class Function;
class CallFunction;
class Block;
class Loop;
class Conditional;
class Object;
class Array;
class AssignField;
class AssignArray;
class AccessField;
class AccessArray;
class CallMethod;

class ASTVisitor
{
public:
    ~ASTVisitor() = default;
    virtual void visit(Print&) {};
    virtual void visit(Integer&) {};
    virtual void visit(Boolean&) {};
    virtual void visit(Null&) {};
    virtual void visit(Variable&) {};
    virtual void visit(Top&) {};
    virtual void visit(AccessVariable&) {};
    virtual void visit(AssignVariable&) {};
    virtual void visit(Function&) {};
    virtual void visit(CallFunction&) {};
    virtual void visit(Block&) {};
    virtual void visit(Loop&) {};
    virtual void visit(Conditional&) {};
    virtual void visit(Object&) {};
    virtual void visit(Array&) {};
    virtual void visit(AssignField&) {};
    virtual void visit(AssignArray&) {};
    virtual void visit(AccessField&) {};
    virtual void visit(AccessArray&) {};
    virtual void visit(CallMethod&) {};
};

#endif //INTERPRETER_ASTVISITOR_H
